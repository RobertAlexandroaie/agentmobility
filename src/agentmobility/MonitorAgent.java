package agentmobility;

import jade.core.Agent;


public class MonitorAgent extends Agent {
    public static FormContainer fc1, fc2;
    
    @Override
    public void setup()
    {
        fc1 = new FormContainer();
        fc1.setName("Container1M");
        fc1.setTitle("Container1M");
        fc1.setLocation(100, 100);
        
        fc2 = new FormContainer();
        fc2.setName("Container2");
        fc2.setTitle("Container2");
        fc2.setLocation(120 + fc1.getSize().width, 100);
        
        fc1.setVisible(true);
        fc2.setVisible(true);
        
        fc1.Write(this.getLocalName() + " started");
        
        this.addBehaviour(new MonitorRefreshBehaviour(this, 250));
        this.addBehaviour(new MonitorReceiveBehaviour(this));
    }
    
    @Override
    public void takeDown()
    {
        MonitorAgent.fc1.Write("Bye from " + this.getLocalName());
    }
}
