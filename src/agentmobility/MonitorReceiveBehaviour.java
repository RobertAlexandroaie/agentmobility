package agentmobility;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

public class MonitorReceiveBehaviour extends CyclicBehaviour {

    public MonitorReceiveBehaviour(Agent a) {
        super(a);
    }

    @Override
    public void action() {
        ACLMessage message = myAgent.receive();

        if (message != null) {
            String[] msg = message.getContent().split("\\|");
            switch (msg[0]) {
                case "Created":
                    if (msg[2].equals("Container1M")) {
                        MonitorAgent.fc1.AddToList(msg[1]);
                        MonitorAgent.fc1.Write(msg[1] + " was created");
                    } else if (msg[2].equals("Container2")) {
                        MonitorAgent.fc2.AddToList(msg[1]);
                        MonitorAgent.fc2.Write(msg[1] + " was created");
                    }
                    break;

                case "Died":
                    if (msg[2].equals("Container1M")) {
                        MonitorAgent.fc1.RemoveFromList(msg[1]);
                        MonitorAgent.fc1.Write(msg[1] + " died");
                    } else if (msg[2].equals("Container2")) {
                        MonitorAgent.fc2.AddToList(msg[1]);
                        MonitorAgent.fc2.Write(msg[1] + " died");
                    }
                    break;

                case "Moving":
                    if (msg[2].equals("Container1M")) {
                        MonitorAgent.fc1.Write(msg[1] + " is about to move from " + msg[2]);
                    } else if (msg[2].equals("Container2")) {
                        MonitorAgent.fc2.Write(msg[1] + " is about to move from " + msg[2]);
                    }
                    break;

                case "Moved":
                    if (msg[2].equals("Container1M")) {
                        MonitorAgent.fc1.Write(msg[1] + " has moved to " + msg[2]);
                        MonitorAgent.fc2.RemoveFromList(msg[1]);
                        MonitorAgent.fc1.AddToList(msg[1]);
                    } else if (msg[2].equals("Container2")) {
                        MonitorAgent.fc2.Write(msg[1] + " has moved to " + msg[2]);
                        MonitorAgent.fc1.RemoveFromList(msg[1]);
                        MonitorAgent.fc2.AddToList(msg[1]);
                    }
                    break;

                case "Hello":
                    if (msg[2].equals("Container1M")) {
                        MonitorAgent.fc1.Write(msg[1] + " says hello from " + msg[2]);
                    } else if (msg[2].equals("Container2")) {
                        MonitorAgent.fc2.Write(msg[1] + " says hello from " + msg[2]);
                    }
                    break;
            }

        } else {
            this.block();
        }
    }
}
