package agentmobility;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.StaleProxyException;

public class JadeHelper {
    
    public static jade.wrapper.AgentContainer CreateContainer(String containerName, Boolean isMainContainer, String hostAddress, String hostPort, String localPort) {
        ProfileImpl p = new ProfileImpl();

        if (containerName.isEmpty() == false) {
            p.setParameter(Profile.CONTAINER_NAME, containerName);
        }

        p.setParameter(Profile.MAIN, isMainContainer.toString());

        if (localPort != null) {
            p.setParameter(Profile.LOCAL_PORT, localPort);
        }

        if (hostAddress.isEmpty() == false) {
            p.setParameter(Profile.MAIN_HOST, hostAddress);
        }

        if (hostPort.isEmpty() == false) {
            p.setParameter(Profile.MAIN_PORT, hostPort);
        }

        if (isMainContainer == true) {
            return jade.core.Runtime.instance().createMainContainer(p);
        } else {
            return jade.core.Runtime.instance().createAgentContainer(p);
        }
    }

    public static jade.wrapper.AgentController CreateAgent(jade.wrapper.AgentContainer container, String agentName, String agentClass, Object[] args) throws StaleProxyException {
        return container.createNewAgent(agentName, agentClass, args);
    }
}

